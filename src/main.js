/*
 * @Author: your name
 * @Date: 2021-11-18 15:39:37
 * @LastEditTime: 2021-11-18 16:24:10
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue3-webpack5-vue-cli4\src\main.js
 */
import {
    createApp
} from 'vue'
import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.less";
import App from './App.vue';

const app = createApp(App);
app.use(Antd).mount('#app');
window.app = app;
window.app.config.productionTip = false;
console.log('输出版本号22：', versionConfig.app_version);