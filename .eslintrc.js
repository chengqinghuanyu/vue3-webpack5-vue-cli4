/*
 * @Author: your name
 * @Date: 2021-10-28 15:40:21
 * @LastEditTime: 2021-11-18 15:47:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue3-webpack5-vue-cli4\.eslintrc.js
 */
module.exports = {
  globals: {
    BMap: true
  },
  root: true,
  env: {
    browser: true,
    node: true,
    "es6": true
  },
  'extends': [
    'plugin:vue/vue3-essential',
    'eslint:recommended'
  ],

  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 6,
    //emcaVersion用来指定你想要使用的 ECMAScript 版本
    sourceType: "module",
    // 设置为 "script" (默认)或"module"（如果你的代码是 ECMAScript 模块)
    ecmaFeature: { //想使用额外的语言特性
      jsx: true, //启用jsx
      globalReturn: true, //在全局作用域下使用return语句
      impliedStrict: true, //启用全局strict mode
      experimentalObjectRestSpread: false
      //启用实验性的object rest/spread properties支持
      //(不建议开启)
    }
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    "space-before-function-paren": 0,
    "vue/html-indent": [
      'error', 4
    ],
    "indent": [
      'error', 4
    ],
    "no-invalid-regexp": "off",
    "no-v-model-argument": "off",
    "no-useless-catch": "off",
    "no-empty": "off",
    "no-useless-escape": "off",
    "vue/no-mutating-props": "off",
    "no-redeclare": "off"
  }
}