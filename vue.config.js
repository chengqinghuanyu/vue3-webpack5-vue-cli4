/*
 * @Author: 尹鹏孝
 * @Date: 2021-10-28 09:50:06
 * @LastEditTime: 2021-11-19 08:56:54
 * @LastEditors: Please set LastEditors
 * @Description: 项目webpack的运行打包配置文件
 * @FilePath: \vue3-webpack5-vue-cli4\vue.confg.js
 */
const path = require("path");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const HappyPack = require('happypack');
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

const LodashModuleReplacementPlugin = require("lodash-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const isProduction = process.env.NODE_ENV === 'production';
require('events').EventEmitter.defaultMaxListeners = 0;
const webpack = require('webpack')
const resolve = dir => path.join(__dirname, dir);
const fs = require('fs')
const rimraf = require('rimraf')
const moment = require("moment");
//设置一个项目版本版本使用moment设置为时间格式
let projectVersion = moment(new Date()).format("YYYY.MM.DD.HH.mm.ss");
// 创建一个 HappyThreadPool，作为所有 loader 共用的线程池
const happyThreadPool = HappyPack.ThreadPool({
    size: 5
});
// cdn预加载使用
const externals = {
    'vue': 'Vue',
    'vue-router': 'VueRouter',
    'vuex': 'Vuex',
    //ant需要这样——antd引入
    'ant-design-vue': 'antd',
    'moment': 'moment',
    'axios': 'axios',
    'echarts': 'echarts',

};

const cdn = {
    // 开发环境
    dev: {
        css: [
            'https://unpkg.com/ant-design-vue@2.1.4/dist/antd.min.css',
            'https://cdn.bootcdn.net/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
        ],
        js: ['https://unpkg.com/vue@3.0.0/dist/vue.global.js',
            'https://cdn.bootcdn.net/ajax/libs/vue-router/4.0.6/vue-router.global.js',
            'https://cdn.bootcdn.net/ajax/libs/vuex/4.0.0/vuex.global.js',
            'https://cdn.jsdelivr.net/npm/ant-design-vue@2.1.4/dist/antd.js',
            'https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js',
            'https://cdn.jsdelivr.net/npm/moment@2.29.1/dist/locale/zh-cn.js',
            'https://cdn.bootcdn.net/ajax/libs/axios/0.21.0/axios.min.js',
            'https://cdn.bootcdn.net/ajax/libs/echarts/5.1.1/echarts.min.js',
        ]
    },
    // 生产环境
    build: {
        css: [
            'https://unpkg.com/ant-design-vue@2.1.4/dist/antd.min.css',
            'https://cdn.bootcdn.net/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
        ],
        js: [
            'https://unpkg.com/vue@3.0.0/dist/vue.global.js',
            'https://cdn.bootcdn.net/ajax/libs/vue-router/4.0.6/vue-router.global.js',
            'https://cdn.bootcdn.net/ajax/libs/vuex/4.0.0/vuex.global.js',
            'https://cdn.jsdelivr.net/npm/ant-design-vue@2.1.4/dist/antd.js',
            'https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js',
            'https://cdn.jsdelivr.net/npm/moment@2.29.1/dist/locale/zh-cn.js',
            'https://cdn.bootcdn.net/ajax/libs/axios/0.21.0/axios.min.js',
            'https://cdn.bootcdn.net/ajax/libs/echarts/5.1.1/echarts.min.js',
        ]
    }
};
module.exports = {
    // filenameHashing 文件名哈希化,生成的静态资源在它们的文件名中包含了 hash 以便更好的控制缓存。同时要求 index 的 HTML 是被 Vue CLI 自动生成的，会自动引入对应的js和css文件。
    filenameHashing: isProduction,
    productionSourceMap: !isProduction, // 生成环境不需要源码映射
    chainWebpack: (config) => {
        config.optimization.delete('splitChunks')
        config.plugins.delete('prefetch');
        // 移除 preload 插件，避免加载多余的资源
        config.plugins.delete('preload');
        config.plugins.delete('named-chunks');
        config.optimization.minimize(true);
        config.resolve.alias
            .set('@', resolve('./src'))
            .set('@components', resolve('./src/components'))
            .set('@assets', resolve('./src/assets'))
            .set('@commonjs', resolve('./src/commonjs'))
            .set('@views', resolve('./src/views'))
        //set第一个参数：设置的别名，第二个参数：设置的路径
        // 修复HMR
        config.resolve.symlinks(true);
        config.plugin("define").tap(args => {
            args[0].versionConfig = JSON.stringify({
                app_version: projectVersion, //通过versionConfig.app_version来访问版本号
            });
            return args;
        });
        config.plugin('html').tap(args => {
            //加载CDN资源在index.html使用模板循环生成cdn配置的内容,根据生产还是测试取不同的cdn资源
            /*<%for (var i in htmlWebpackPlugin.options.cdn && htmlWebpackPlugin.options.cdn.css) {%>
                <link href="<%= htmlWebpackPlugin.options.cdn.css[i] %>" rel="preload" as="style" />
                    <link href = "<%= htmlWebpackPlugin.options.cdn.css[i] %>"rel = "stylesheet" />
                    <}%>*/
            /*<% for (let i in htmlWebpackPlugin.options.cdn && htmlWebpackPlugin.options.cdn.js) { %>
                    <script src="<%= htmlWebpackPlugin.options.cdn.js[i] %>"></script>
                    <% } %> */
            args[0].cdn = isProduction ? cdn.build : cdn.dev;

            args[0].title = 'webpack5-vue/cli4-vue3项目';
            return args
        });
        if (isProduction) {
            //压缩图片-start------------
            config.module.rule('images')
                .test(/\.(png|jpe?g|gif)(\?.*)?$/)
                .use('url-loader')
                .loader('url-loader')
                .tap(options => ({
                    limit: 1024, // 稍微改大了点
                    fallback: {
                        loader: require.resolve('file-loader'),
                        options: {
                            // 在这里修改file-loader的配置
                            // 直接把outputPath的目录加上，虽然语义没分开清晰但比较简洁
                            name: `/img/[name].[hash:8].[ext]`,
                            esModule: false, //低版本默认为false，高版本默认为true 这里为6.2.0为高本版本所以要手动设置为false 
                            // 从生成的资源覆写 filename 或 chunkFilename 时，`assetsDir` 会被忽略。
                            // 因此别忘了在前面加上静态资源目录，即assetsDir指定的目录，不然会直接在dist文件夹下
                            //outputPath: `${__dirname}/dist/img`
                        }
                    }
                }))
                .end()
                .use('image-webpack-loader')
                .loader('image-webpack-loader')
                .options({
                    bypassOnDebug: true,
                    mozjpeg: {
                        progressive: true,
                        quality: 50
                    }, // 压缩JPEG图像
                    optipng: {
                        enabled: true
                    }, // 压缩PNG图像
                    // pngquant: {
                    //     quality: [0.5, 0.65],
                    //     speed: 4
                    // }, // 压缩PNG图像
                    gifsicle: {
                        interlaced: false
                    } // 压缩GIF图像
                })
                .end()
                .enforce('post');
        }

        //压缩图片-end------------
        const types = ["vue-modules", "vue", "normal-modules", "normal"];
        types.forEach((type) => addStyleResource(config.module.rule("less").oneOf(type)));

        //完成以后追加统计-------
        config
            .plugin('webpack-bundle-analyzer')
            .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin).init(Plugin => new Plugin({
                analyzerMode: "server",
                analyzerHost: "127.1.0.1",
                analyzerPort: 1888, // 运行后的端口号
                reportFilename: "report.html",
                defaultSizes: "parsed",
                openAnalyzer: true,
                generateStatsFile: false,
                statsFilename: "stats.json",
                statsOptions: null,
                logLevel: "info"
            }))
    },
    configureWebpack: config => {
        const plugins = [];
        plugins.push(new CssMinimizerPlugin());
        if (isProduction) {
            // 服务器也要相应开启gzip,这块需要nginx服务器也开启gzip压缩否则不生效
            plugins.push(
                new CompressionWebpackPlugin({

                    algorithm: 'gzip',
                    test: /\.(js|css|html|svg)$/, // 匹配文件名
                    threshold: 10 * 1024, // 对超过10k的数据压缩
                    deleteOriginalAssets: false, // 不删除源文件
                    minRatio: 0.8, // 压缩比
                    deleteOriginalAssets: false // 删除原文件
                })
            )

            //jquery
            /*  plugins.push(new webpack.ProvidePlugin({
                 $: 'jquery',
                 jQuery: 'jquery'
             })) */
            plugins.push(new LodashModuleReplacementPlugin());
            plugins.push(new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /zh-cn$/));
            plugins.push(new HappyPack({
                id: 'babel', // 与loader 配置项对应
                threads: 4, // 开启多少个进程
                //loaders: ['babel-loader'] //用什么loader处理
                loaders: [{
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true
                    }
                }],
                threadPool: happyThreadPool
            }));

            // 开启分离js
            config.optimization = {
                runtimeChunk: 'single',
                minimize: true,
                moduleIds: 'deterministic',
                chunkIds: 'deterministic',
                mangleExports: 'deterministic',
                innerGraph: true,
                minimizer: [
                    new TerserPlugin({
                        test: /\.js(\?.*)?$/i,
                        //include: /src/,
                        //exclude: /node_modules/,
                        //parallel: true, //启用多进程并发运行并设置并发运行次数,开启必须在uglifyJsMinify用require的方式
                        minify: TerserPlugin.uglifyJsMinify, //是一款集 JavaScript 解析器，压缩器，美化器于一身的工具集。
                        //minify: TerserPlugin.esbuildMinify,

                        /*  使用require("uglify-js").minify(file, uglifyJsOptions);打包了一早上也没完事，就先不用了。
                        
                        minify: (file, sourceMap) => {
                                 // https://github.com/mishoo/UglifyJS2#minify-options
                                 //https://segmentfault.com/a/1190000010874406
                                 const uglifyJsOptions = {
                                   
                            // parse 解释
                            // compress 压缩
                            // mangle 混淆
                            // beautify 美化
                            // minify 最小化
                            // CLI 命令行工具
                            // sourcemap 编译后代码对源码的映射， 用于网页调试
                            // AST 抽象语法树
                            // name 名字， 包括变量名、 函数名、 属性名
                            // toplevel 顶层作用域
                            // unreachable 不可达代码
                            // option 选项
                            // STDIN 标准输入， 指在命令行中直接输入
                            // STDOUT 标准输出
                            // STDERR 标准错误输出
                            // side effects函数副作用， 即函数除了返回外还产生别的作用， 比如改了全局变量
                            warnings: false,
                            parse: {
                                // parse options
                            },
                            compress: {
                                // compress options
                                drop_console: true,
                                dead_code: true,
                                unused: true,
                            },
                            // mangle: {
                            //     // mangle options

                            //     properties: {
                            //         // mangle property options
                            //     }
                            // },
                            mangle: true,
                            output: {
                                // output options
                                ast: true,
                                code: false // optional - faster if false
                            },
                            sourceMap: {
                                // source map options
                            },
                            nameCache: null, // or specify a name cache object
                            toplevel: false,
                            ie8: false,
                        };

                        if (sourceMap) {
                            uglifyJsOptions.sourceMap = {
                                content: sourceMap,
                            };
                        }

                        return require("uglify-js").minify(file, uglifyJsOptions);
                    }, */
                        terserOptions: {
                            mangle: true, // 混淆，默认也是开的，mangle也是可以配置很多选项的
                            compress: {
                                drop_console: true, //传true就是干掉所有的console.*这些函数的调用.
                                drop_debugger: true, //干掉那些debugger;
                                pure_funcs: ['console.log'], // 如果你要干掉特定的函数比如console.info ，又想删掉后保留其参数中的副作用，那用pure_funcs来处理
                                //passes: 2,
                            }
                        },
                        extractComments: false, //(blooen|string剥离 all 或 some)RegExp|Function|Object启用/禁用剥离注释功能

                    }),
                ],
                splitChunks: {
                    chunks: 'initial', // 分割方式 async, all ，initial
                    maxInitialRequests: 30, // 初始模块的最大分割数
                    maxAsyncRequests: 1,
                    minSize: 1000, //分割后的文件最小值
                    automaticNameDelimiter: '~', // 分割后的名字用什么符号链接
                    cacheGroups: {
                        defaultVendors: {
                            reuseExistingChunk: true,
                            test: /[\\/]node_modules[\\/]/,
                            name(module) {
                                // 排除node_modules 然后吧 @ 替换为空 ,考虑到服务器的兼容
                                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]
                                return `npm.${projectVersion}${packageName.replace('@', '')}`
                            }
                        },
                        //公用模块抽离
                        common: {
                            chunks: 'initial',
                            minSize: 0, //大于0个字节
                            minChunks: 2, //抽离公共代码时，这个代码块最小被引用的次数
                        },
                        venxx: {
                            test: /vue/,
                            name: 'vuevendors'
                        },
                        runtimeChunk: {
                            name: 'manifest' + projectVersion,
                        }
                    }
                }
            };
            // 取消webpack警告的性能提示
            config.performance = {
                hints: 'warning',
                // 入口起点的最大体积
                maxEntrypointSize: 1000 * 10000,
                // 生成文件的最大体积
                maxAssetSize: 1000 * 30000,
                // 只给出 js 文件的性能提示
                assetFilter: function (assetFilename) {
                    return assetFilename.endsWith(`.js`)
                }
            };
            //现在注销这句是CDN引入后项目报错地方太多暂时没敢开启cdn，目前先留着
            //config.externals = externals
            config.plugins.push({
                apply: compiler => {
                    //自定义插件MyPlugin-------
                    compiler.hooks.done.tap('MyPlugin', options => {
                        rimraf('./dist/' + projectVersion, function (err) {
                            if (err) console.log(err)
                            console.log(`----删除文件夹${projectVersion}----`);
                            fs.readdir('./dist', (err, files) => {
                                if (err) console.log('----读取dist文件夹出错----', err)
                                console.log(`----dist文件夹${files}----`);
                            })
                        })
                        return options
                    })
                }
            })
        }


        return {
            plugins
        }
    },
    css: {
        extract: !isProduction,
        //如果css使用sourceMap需要关闭extract
        sourceMap: !isProduction,
        loaderOptions: {
            less: {
                // 属性值包裹在lessOptions内,在less里面引入样式最终结果是请求public/images/*** */
                lessOptions: {
                    modifyVars: {
                        "primary-color": "#4890ff", // 全局主色
                        "link-color": "#4890ff", // 链接色
                        "success-color": "#99c41a", // 成功色
                        'warning-color': '#66ad14', // 警告色
                        'error-color': '#f5282d', // 错误色
                        'font-size-base': '14px', // 主字号
                        'heading-color': "rgba(0, 0, 0, 0.85)", // 标题色
                        'text-color': "rgba(0, 0, 0, 0.65)", // 主文本色
                        'text-color-secondary': "rgba(0, 0, 0, 0.45)", // 次文本色
                        'disabled-color': 'rgba(0, 0, 0, 0.25)', // 失效色
                        'border-radius-base': '4px', // 组件/浮层圆角
                        'border-color-base': "#12d9d9", // 边框色
                        'box-shadow-base': '0 2 px 8 px rgba(0, 0, 0, 0.15)', // 浮层阴影
                    },
                    javascriptEnabled: true,
                }
            },
        },
        extract: { // 打包后css文件名称添加当前时间戳
            filename: `css/[name].${projectVersion}.css`,
            //在package.jon里面配置，从中获取
            // chunkFilename: `css/chunk.[id].${process.env.npm_package_lastTwoVersion}.css`,
            //通过moment格式化当前时间获取
            chunkFilename: `css/chunk.[id].${projectVersion}.css`,
        },
    },
    outputDir: 'dist', //输出目录
    lintOnSave: false,
    devServer: {
        port: 2021,
        //自动打开浏览器
        // open: true,
        // public: require('os').networkInterfaces()[Object.keys(require('os').networkInterfaces())[0]][1].address + ':1988',
        proxy: {
            "/oms": {
                target: "http://10.0.7.31:8001/", // 目标地址 //正式oms.gongheyuan.com
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    "^/oms": "",
                },
            },
            "/signalr": {
                target: "http://10.0.7.31:8001/", // /signalr代理挺恶心的是后台写死的。
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    "^/oms": "/oms/signalr",
                },
            },
            "/doc": {
                target: "http://10.0.7.50:5007",
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    "^/doc": "",
                },
            },
            "/owf": {
                target: "http://10.0.7.31:8307",
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    "^/owf": "",
                },
            },
            "/sso": {
                //单点登录
                target: "http://10.0.7.32:15000",
                changeOrigin: true,
                pathRewrite: {
                    "^/sso": "/",
                },
            },
            "/oup": {
                target: "http://10.0.7.36:5007/",
                changeOrigin: true, //后台对接 王福成
                pathRewrite: {
                    "^/oup": "",
                },
            },
            "/portal": {
                target: "http://10.0.7.50:5005",
                changeOrigin: true,
                pathRewrite: {
                    "^/portal": "/",
                },
            },
            "/social": {
                target: "http://10.0.7.31:8111",
                changeOrigin: true,
                pathRewrite: {
                    "^/social": "/",
                },
            },
            "/cmp": {
                target: "http://10.0.7.31:8305/",
                changeOrigin: true,
                pathRewrite: {
                    "^/cmp": "/",
                },
            },
            "/new-shop": {
                target: "http://10.0.7.50:5053", // 商城相关
                changeOrigin: true,
                pathRewrite: {
                    "^/new-shop": "/",
                },
            },
            "/ass-ms": {
                target: "http://10.0.7.31:8305", // cmp OA
                changeOrigin: true,
                pathRewrite: {
                    "^/ass-ms": "/",
                },
            },
            "/old": {
                target: "https://yctestoms.gongheyuan.com", // cmp OA
                changeOrigin: true,
                pathRewrite: {
                    "^/old": "/",
                },
            },
            "/oa-mmse": {
                target: "http://10.0.7.31:8043/",
                changeOrigin: true,
                pathRewrite: {
                    "^/oa-mmse": "/",
                },
            },
            "/hms": {
                target: "http://10.0.7.31:8500/",
                changeOrigin: true,
                pathRewrite: {
                    "^/hms": "/",
                },
            },
        },
    },

};


function addStyleResource(rule) {
    rule.use("style-resource")
        .loader("style-resources-loader")
        .options({
            patterns: [
                path.resolve(__dirname, "src/assets/css/index.less"), // 需要全局导入的less
            ],
        });
}
/*

'use strict'
const path = require('path')
const defaultSettings = require('./src/settings.js')
const CompressionPlugin = require('compression-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
function resolve(dir) {
  return path.join(__dirname, dir)
}

const name = defaultSettings.title // 网址标题
const port = 8013 // 端口配置

// All configuration item explanations can be find in https://cli.vuejs.org/config/
module.exports = {
  // hash 模式下可使用
  // publicPath: process.env.NODE_ENV === 'development' ? '/' : './',
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      '/api': {
        target: process.env.VUE_APP_BASE_API,
        changeOrigin: true,
        pathRewrite: {
          '^/api': 'api'
        }
      },
      '/auth': {
        target: process.env.VUE_APP_BASE_API,
        changeOrigin: true,
        pathRewrite: {
          '^/auth': 'auth'
        }
      }
    }
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    name: name,
    resolve: {
      alias: {
        '@': resolve('src'),
        '@crud': resolve('src/components/Crud')
      }
    },
    plugins: [
      // https://www.ydyno.com/archives/1260.html 使用gzip解压缩静态文件
      new CompressionPlugin({
        test: /\.(js|css|html)?$/i, // 压缩文件格式
        filename: '[path].gz[query]', // 压缩后的文件名
        algorithm: 'gzip', // 使用gzip压缩
        minRatio: 0.8, // 压缩率小于1才会压缩
        threshold: 10240,
      }),
      new BundleAnalyzerPlugin(),
    ]
  },
  chainWebpack(config) {
    config.plugins.delete('preload') // TODO: need test
    config.plugins.delete('prefetch') // TODO: need test

    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/assets/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/assets/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    // set preserveWhitespace
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => {
        options.compilerOptions.preserveWhitespace = true
        return options
      })
      .end()

    config
      // https://webpack.js.org/configuration/devtool/#development
      .when(process.env.NODE_ENV === 'development',
        config => config.devtool('cheap-source-map')
      )

    config
      .when(process.env.NODE_ENV !== 'development',
        config => {
          config
            .plugin('ScriptExtHtmlWebpackPlugin')
            .after('html')
            .use('script-ext-html-webpack-plugin', [{
            // `runtime` must same as runtimeChunk name. default is `runtime`
              inline: /runtime\..*\.js$/
            }])
            .end()
          config
            .optimization.splitChunks({
              chunks: 'all',
              // cacheGroups: {
              //   libs: {
              //     name: 'chunk-libs',
              //     test: /[\\/]node_modules[\\/]/,
              //     priority: 10,
              //     chunks: 'initial' // only package third parties that are initially dependent
              //   },
              //   echarts: {
              //       test: /[\\/]node_modules[\\/]echarts[\\/]/,
              //       name: 'echarts',
              //       chunks: 'all',
              //       priority: 30,
              //   },
              //    pdfjs: {
              //      test: /[\\/]node_modules[\\/]pdfjs-dist[\\/]/,
              //      name: 'pdfjs',
              //      chunks: 'all',
              //      priority: 30,
              //    },
              //    jspdf: {
              //      test: /[\\/]node_modules[\\/]jspdf[\\/]/,
              //      name: 'jspdf',
              //      chunks: 'all',
              //      priority: 30,
              //    },
              //    xlsx: {
              //      test: /[\\/]node_modules[\\/]xlsx[\\/]/,
              //      name: 'xlsx',
              //      chunks: 'all',
              //      priority: 30,
              //    },
              //    mavonEditor: {
              //      test: /[\\/]node_modules[\\/]mavon-editor[\\/]/,
              //      name: 'mavonEditor',
              //      chunks: 'all',
              //      priority: 30,
              //    },
              //     wangEditor: {
              //       test: /[\\/]node_modules[\\/]wangEditor[\\/]/,
              //       name: 'mavonEditor',
              //       chunks: 'all',
              //       priority: 30,
              //     },
              //     canvg: {
              //       test: /[\\/]node_modules[\\/]canvg[\\/]/,
              //       name: 'canvg',
              //       chunks: 'all',
              //       priority: 30,
              //     },
              //      html2canvas: {
              //        test: /[\\/]node_modules[\\/]html2canvas[\\/]/,
              //        name: 'html2canvas',
              //        chunks: 'all',
              //        priority: 30,
              //      },
              //       elTableInfiniteScroll: {
              //          test: /[\\/]node_modules[\\/]el-table-infinite-scroll[\\/]/,
              //            name: 'elTableInfiniteScroll',
              //            chunks: 'all',
              //            priority: 30,

              //      },
              //      webSteamPolyfill: {
              //        test: /[\\/]node_modules[\\/]web-steam-polyfill[\\/]/,
              //        name: 'webSteamPolyfill',
              //        chunks: 'all',
              //        priority: 30,

              //      },
              //      vueTreeSelect: {
              //        test: /[\\/]node_modules[\\/]vue-treeselect[\\/]/,
              //        name: 'vueTreeSelect',
              //        chunks: 'all',
              //        priority: 30,

              //      },
              //       vueTour: {
              //         test: /[\\/]node_modules[\\/]vue-tour[\\/]/,
              //         name: 'vueTour',
              //         chunks: 'all',
              //         priority: 30,

              //       },



              //   elementUI: {
              //     name: 'chunk-elementUI', // split elementUI into a single package
              //     priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
              //     test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
              //   },
              //   commons: {
              //     name: 'chunk-commons',
              //     test: resolve('src/components'), // can customize your rules
              //     minChunks: 3, //  minimum common number
              //     priority: 5,
              //     reuseExistingChunk: true
              //   }
              // }
              cacheGroups:{
                  //优化重要的方式2
                vendor: {
                  test: /[\\/]node_modules[\\/]/,
                  name(module) {
                    // get the name. E.g. node_modules/packageName/not/this/part.js
                    // or node_modules/packageName
                    const packageName = module.context.match(
                      /[\\/]node_modules[\\/](.*?)([\\/]|$)/
                    )[1];
                    // npm package names are URL-safe, but some servers don't like @ symbols
                    return `npm.${packageName.replace("@", "")}`;
                  },
                  chunks: "all",
                  enforce: true,
                  priority: 10,
                  minSize: 100000, // 100KB
                  maxSize: 300000,
                  reuseExistingChunk: true,
                },
              }
            })
          config.optimization.runtimeChunk('single')
          config.optimization.minimizer=[
              new TerserPlugin({
                terserOptions: {
                  compress: {
                    drop_console: true, // remove console statement
                  },
                },
              }),
            ]
        }
      )
  },
  transpileDependencies: [
    'vue-echarts',
    'resize-detector'
  ]
}
*/
