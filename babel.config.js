/*
 * @Author: your name
 * @Date: 2021-10-28 09:46:47
 * @LastEditTime: 2021-11-18 15:46:42
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue3-webpack5-vue-cli4\babel.config.js
 */
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: ["lodash"]
}