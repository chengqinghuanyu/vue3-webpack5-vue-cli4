/*
 * @Author: 尹鹏孝
 * @Date: 2021-11-17 15:00:33
 * @LastEditTime: 2021-11-18 13:58:05
 * @LastEditors: Please set LastEditors
 * @Description: 
 * 总结:
        1) webpack.config.js > postcss.config.js
        2) postcss.config.js > .browserslistrc
        3) postcss.config.js > package.json
        4) package.json和.browserslistrc只能定义一个, 否则会冲突
        结论: webpack.config.js在查找browserslist是从内向外找的, 找到就会停下来
 * @FilePath: \yc.oms.apt.optz\src\postcss.config.js
 */
module.exports = {
    plugins: {
        'autoprefixer': {
            // overrideBrowserslist: ['Android >= 4.0', 'iOS >= 7']
            overrideBrowserslist: ['last 2 version', '>1%', 'ios >= 7', 'not dead']
        },
        'postcss-pxtorem': {
            rootValue: 16, //结果为：设计稿元素尺寸/16，比如元素宽320px,最终页面会换算成 20rem
            propList: ['*']
        }
    }
}